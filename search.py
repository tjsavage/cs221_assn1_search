# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called 
by Pacman agents (in searchAgents.py).
"""

import util

class Frontier(util.PriorityQueueWithFunction):
	def __init__(self, costfn):
		self.frontier_set = set()
		util.PriorityQueueWithFunction.__init__(self,costfn)
					
	def contains(self, item):
		return item in self.frontier_set
	
	def push(self, item):
		util.PriorityQueueWithFunction.push(self, item)
		self.frontier_set.add(item[len(item) - 1][0])
	
	def pop(self):
		item = util.PriorityQueueWithFunction.pop(self)
		if item[len(item) - 1][0] in self.frontier_set:
			self.frontier_set.remove(item[len(item) - 1][0])
		return item
					
	def is_empty(self):
		return len(self.frontier_set) == 0
					
	def __str__(self):
		return str(self.frontier_set)
                
class SearchProblem:
  """
  This class outlines the structure of a search problem, but doesn't implement
  any of the methods (in object-oriented terminology: an abstract class).
  
  You do not need to change anything in this class, ever.
  """
  
  def getStartState(self):
     """
     Returns the start state for the search problem 
     """
     util.raiseNotDefined()
    
  def isGoalState(self, state):
     """
       state: Search state
    
     Returns True if and only if the state is a valid goal state
     """
     util.raiseNotDefined()

  def getSuccessors(self, state):
     """
       state: Search state
     
     For a given state, this should return a list of triples, 
     (successor, action, stepCost), where 'successor' is a 
     successor to the current state, 'action' is the action
     required to get there, and 'stepCost' is the incremental 
     cost of expanding to that successor
     """
     util.raiseNotDefined()

  def getCostOfActions(self, actions):
     """
      actions: A list of actions to take
 
     This method returns the total cost of a particular sequence of actions.  The sequence must
     be composed of legal moves
     """
     util.raiseNotDefined()
           
def genericSearch(problem, costFn):
	frontier = Frontier(costFn)
	start_node = problem.getStartState()
	solution = []
	explored = set()

	for configuration in problem.getSuccessors(start_node):
		frontier.push([configuration])
	
	while not frontier.is_empty():
		path = frontier.pop()
		node = path[len(path)-1][0]
		explored.add(node)

		if problem.isGoalState(node):
			return [c[1] for c in path]
		
		for configuration in problem.getSuccessors(node):
			if not frontier.contains(configuration[0]) and configuration[0] not in explored:
				frontier.push(path + [configuration])
	
	return []

def tinyMazeSearch(problem):
  """
  Returns a sequence of moves that solves tinyMaze.  For any other
  maze, the sequence of moves will be incorrect, so only use this for tinyMaze
  """
  from game import Directions
  s = Directions.SOUTH
  w = Directions.WEST
  return  [s,s,w,s,w,w,s,w]

def depthFirstSearch(problem):
	"""
	Search the deepest nodes in the search tree first
	[2nd Edition: p 75, 3rd Edition: p 87]
	
	Your search algorithm needs to return a list of actions that reaches
	the goal.  Make sure to implement a graph search algorithm 
	[2nd Edition: Fig. 3.18, 3rd Edition: Fig 3.7].
	
	To get started, you might want to try some of these simple commands to
	understand the search problem that is being passed in:
	
	print "Start:", problem.getStartState()
	print "Is the start a goal?", problem.isGoalState(problem.getStartState())
	print "Start's successors:", problem.getSuccessors(problem.getStartState())
	"""
	"*** YOUR CODE HERE ***" 
	
	return genericSearch(problem, lambda a:0)	
		
	

def breadthFirstSearch(problem):
  """
  Search the shallowest nodes in the search tree first.
  [2nd Edition: p 73, 3rd Edition: p 82]
  """
  "*** YOUR CODE HERE ***"
  return genericSearch(problem, lambda p:len(p))
      
def uniformCostSearch(problem):
  "Search the node of least total cost first. "
  "*** YOUR CODE HERE ***"
  return genericSearch(problem, lambda p:p[len(p) - 1][2])

def nullHeuristic(state, problem=None):
  """
  A heuristic function estimates the cost from the current state to the nearest
  goal in the provided SearchProblem.  This heuristic is trivial.
  """
  return 0

def aStarSearch(problem, heuristic=nullHeuristic):
  "Search the node that has the lowest combined cost and heuristic first."
  "*** YOUR CODE HERE ***"
  def costFunction(path):
  	c= heuristic(path[len(path)-1][0], problem)
  	print "Cost:",c
  	return c
  
  return genericSearch(problem, costFunction)
    
  
# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
